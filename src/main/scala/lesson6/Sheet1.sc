//functions are first class citizens in Scala

import scala.math._
val num = 3.14
val fun = ceil(_)

//Call a function

fun(num)

//Pass a function to another function : here passing fun to map.
Array(3.14,1.42,2.0).map(fun)

//map can also be passed an anonymous function (i.e. a function without a name)
//map is called a higher order function, because it is a fn that consumes another fn.

Array(3.14,1.42,2.0).map((x:Double) => x* 3)


//function/method with function argument
def valueAtOneQuart(f: (Double) => Double) = f(0.25)

valueAtOneQuart(ceil(_))

//example of a function that takes a double and produces a function

def mulBy(factor: Double) = (x: Double)=> factor * x

val tripleIt = mulBy(3)

//tripleIt can be used as a function
tripleIt(4)

