//Explore a dataset with higher order function

//DataSet
val zones = java.util.TimeZone.getAvailableIDs


//Get rid of the continents

val resultDataSet1 = zones.map(x => x.split("/")).filter(ar => ar.length > 1).map(a => a(1))


//example of reduceLeft


def computeMulTill(n: Int): Int = {
  1.to(n).reduceLeft(_ * _)
}

computeMulTill(5)


//Write a function to calculate 2 power n  without a loop

def nPower(n: Int)= {
  //Math.pow(2,n)
  1.to(n).map(x => 2).reduceLeft(_*_)
}

nPower(5)



//Example of String concat using reduceLeft

def concat(s: Seq[String],sep: String) = {
  s.reduceLeft(_+sep+_)
}

concat(Seq("Hello", "World", "Good"),"~")
