//map applies a function to each element of a predicate
(1 to 9).map(0.1 * _)


//filter retains elements that fulfil a predicate
(1 to 9).filter(_ % 2 ==0)

//reduceLeft is a binary function, going frm left to right & takes 2 arguments.
(1 to 9).reduceLeft(_+_)

/**
  * Currying turns a function that takes two arguments into a function
  * that takes one argument.
  * that function in turn returns a function that consumes the second argument.
  */

def mul(x: Int, y: Int) = x * y
//curried version of above function
def mulOneAtATime(x: Int) = (y: Int) => x * y

mulOneAtATime(3)(4)


//syntactic sugar for currying
def mul1(x: Int)(y: Int) = x * y