val a = 6 * 7

//a = 43  //Reassignment to val not allowed.

val b: BigInt = 6 * 7; //semi-colon is allowed , but not used.

// Raising b to the power of 1000
b.pow(1000)