/**
  * Pattern Matching Exercise: Write a function 'swap' that receives a pair of integers and returns
  * the pair with the components swapped.
  */


def swap(p: (Int,Int)):(Int,Int) = p match {
  case (a,b) => (b,a)
  case _ => throw new IllegalArgumentException
}

swap((10,2))
swap((3,11))



/**
  * Write a function swap that receives an Array[Int] and returns the Array with the first two
  * elements swapped, provided the length is atleast 2.
  */


def swap2(arr: Array[Int]): Array[Int] = arr match{

  case Array(x,y,rest @_*) if arr.length>2  => Array(y,x) ++ rest
  case _ => throw new IllegalArgumentException

}


swap2(Array(1,2,4,5))
//swap2(Array(1))

/**
  * A store sells items. Some are articles. Others are bundles of articles.
  */

abstract class Item
case class Article(description: String, price: Double) extends Item
case class Bundle(description: String, discount: Double, items: Item*) extends Item

//example of an Article instantiation
val bookArticle = Article("Scala for the Impatient",39.95)

//Example of a bundle instatiation
val gift = Bundle("Book and Whiskey",10.0,bookArticle,Article("Old Potrero Straight Rye Whiskey",79.97))


def priceCalc(it: Item): Double = it match {
  case Article(desc,price) => price
  case Bundle(desc,disc,rest @_*)  => rest.map(art => priceCalc(art)).sum - disc


}

priceCalc(bookArticle)
priceCalc(gift)