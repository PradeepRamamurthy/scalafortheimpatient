//Collection Exercises - Arrays, Maps, Tuples

//Exercise 1: Given an array buffer with positive and negative integers, please remove all but the first negative number.

import scala.collection.mutable.ArrayBuffer


def removeAllButFirstNegative(buf: ArrayBuffer[Int]) = {

  val negIndices  = for(i <- 0 until buf.length if buf(i) <0) yield i
  val indicesToRemove = negIndices.drop(1)
  //Get result by filtering out all elements present in the indicesToRemove.
  val result = for(i <- 0 until buf.length if !indicesToRemove.contains(i))yield buf(i)
  result

}

removeAllButFirstNegative(ArrayBuffer(-1,0,-2,9,10,34,-22,-15,79))

//Word count using a Map.


def countAlice: scala.collection.mutable.Map[String,Int] = {
  val in = new java.util.Scanner(new java.net.URL("http://horstmann.com/presentations/livelessons-scala-2016/alice30.txt").openStream())
  val count = scala.collection.mutable.Map[String,Int]()

  while(in.hasNext()){
    val word = in.next()
    count(word) = count.getOrElse(word,0) + 1
  }
count
}
val count = countAlice
count("Alice")
count("Rabbit")


//Using GroupBy method of the Array class

val words = Array("Mary","had","a","little","lamb","its","fleece","was","white","as","snow","and","everywhere","there","Mary","went","the","lamb","was","sure","to","go")
//clustering the words by first letter
words.groupBy(_.substring(0,1))
//clustering the words by the length of the string
words.groupBy(_.length)

//Partitions & Zips

"New York".partition(_.isUpper)

//Solve using partition when ordering is not important - remove all but the first negative number.

var (neg, pos) = ArrayBuffer(-1,0,-2,9,10,34,-22,-15,79).partition(_ < 0)
var result = pos
result += neg(0)

//zip method takes two collections of the same length and "zips" them together into collection of tuples

val symbols = Array("<","-",">")
val counts = Array(2,10,2)
val zipped = symbols.zip(counts)

zipped.map(x => x._1 * x._2).mkString("")
