//Example of a class

class Point(val x: Double, val y: Double) {

  def move(dx: Double,dy: Double) = new Point(x+dx,y+dy)
  def distanceFromOrigin = math.sqrt(x*x + y*y)
  override def toString = f"(${x},${y})"


}

val p = new Point(3,4)
p.move(10,20)
p.distanceFromOrigin
p.x
p.y


/**
  * Write a class Time with read only fields hours and minutes,
  * a method toString, and a method before(other: Time) that checks
  * whether this time comes before the other. A time object should be
  * constructed as a new Time(h,m), where h is between 0 and 23 and m is between
  * 0 and 59. If there aren't as expected, throw an IllegalArgument exception
  */


class Time(val h: Int, val m : Int = 0) {
  if ((h < 0 || h > 23) || ((m < 0) || (m>59))){throw new IllegalArgumentException}

  private val timeInSeconds = h*60*60 + m*60

  def before(other: Time): Boolean = {
    if ((this.timeInSeconds < other.timeInSeconds)) true else false
  }

  override def toString ={
     f"(${h}:${m}%02d)"
  }
}

new Time(13,29)
new Time(13)
//new Time(24,29)
val ot = new Time(18,33)
new Time(18,34).before(ot)

/**
  * implement the before method above ,such that one can call t1 < t2.
  * Also add a '-' method such that it yields number of minutes between them.
  * Also, make the Time such that a time object can be constructed without calling new method.
  */


class Time1(val h: Int, val m : Int = 0) {
  if ((h < 0 || h > 23) || ((m < 0) || (m>59))){throw new IllegalArgumentException}

  private val timeInSeconds = h*60*60 + m*60

  def before(other: Time1): Boolean = {
    print(other.timeInSeconds)
    print(this.timeInSeconds)
    if ((this.timeInSeconds < other.timeInSeconds)) true else false
  }

  override def toString ={
    f"(${h}:${m}%02d)"
  }


  def -(other: Time1) = (h*60 + m) - (other.h*60+other.m)

  def <(other: Time1) = { if((h*60 + m) < (other.h*60+other.m)) true else false}

}

//creating a companion object , so that new keyword is avoided while creating an object.
object Time1 {
  def apply(x: Int, y: Int=0) = {
    new Time1(x, y)
  }
}

Time1(11,29)
Time1(11)
//new Time(24,29)
val ot1 = Time1(20,33)
Time1(12,29).before(ot1)

Time1(19,0) < Time1(12,30)
