/**
  * Write a fn. that tests whether a function is a vowel or not (just lower case)
  * Write without an if statement.
  *
  */


def isVowel(ch: Char): Boolean = {
  Seq('a','e','i','o','u').contains(ch)
}

isVowel('a')
isVowel('z')


/**
  * Write a function that given a String, return a string of all its vowels.
  * Use the for loop
  */

def vowels(s: String): String = {
  var result = new StringBuilder("")
  for(ch <- s if "aeiou".contains(ch)) {
    result += ch
  }
  result.toString()
}

vowels("Hello")
vowels("ballaclava")
vowels("DMZ")


//Same problem with a yield

def vowelsYield (s: String): Seq[Char] = {
  for(ch <- s if "aeiou".contains(ch)) yield ch
}

vowelsYield("Hello")
vowelsYield("ballaclava")
vowelsYield("DMZ")


//vowels function implemented as a recursive function

def vowelsRec(s: String): String = {
  val vowel = "aeiou"
  var result = ""

  if (s.length == 0) {
    result
  }
  else{
    if(vowel.contains(s.head)) result += s.head else vowelsRec(s.tail)
    result
  }

}

vowelsYield("Hello")
vowelsYield("ballaclava")
vowelsYield("DMZ")


//Implement vowel with a traditional while loop.

def vowelsTrad(s: String) = {
  val vowels = "aeiou"
  var i = 0
  var result = ""
  while(i < s.length){
    if (vowels.contains(s(i))) {
      result += s(i)
    }
      i += 1

  }
  result
}

vowelsTrad("Hello")
vowelsTrad("ballaclava")
vowelsTrad("DMZ")

//With named parameter

def vowelsParam(s: String ,vowel: String = "aeiou", ignoreCase: Boolean = true): String = {
  var result = new StringBuilder("")
  for(ch <- s)  {

    if (ignoreCase && vowel.contains(ch.toLower)) {
      result += ch
    }
      
    else if (vowel.contains(ch) ) {
      result += ch
    }
  }
  result.toString()
}

vowelsParam("Hello")
vowelsParam("ballaclava")
vowelsParam("DMZ")

vowelsParam("HEllo",ignoreCase = false)
vowelsParam("ballaclavA",ignoreCase = false)
